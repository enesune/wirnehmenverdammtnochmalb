package data;

import java.io.IOException;
import java.net.Socket;

import logic.Logic;

public class Netzwerk {
	private String adresse = "localhost";
	private int port = 4555;
	private Socket clientSocket;
	private boolean connected = false;
	private String benutzername = "";
	private String passwort = "";
	private Logic logic;
	
	public Netzwerk(Logic logic) {
		this.logic = logic;
	}
	
	public Logic getLogic() {
		return logic;
	}

	public boolean isConnected() {
		return connected;
	}

	public void setConnected(boolean connected) {
		this.connected = connected;
	}

	public String getBenutzername() {
		return benutzername;
	}

	public void setBenutzername(String benutzername) {
		this.benutzername = benutzername;
	}

	public String getPasswort() {
		return passwort;
	}

	public void setPasswort(String passwort) {
		this.passwort = passwort;
	}

	public Socket getClientSocket() {
		return clientSocket;
	}

	public boolean connect(String name, String passwort) {
		try {
			benutzername = name;
			this.passwort = passwort;
			clientSocket = new Socket(adresse, port);
			new Listen(this, "").start();
			connected = true;
		} catch (Exception e) {
			e.printStackTrace();
			connected = false;
			return false;
		}
		return true;
	}
	
	public void disconnect() {
		try {
			if (clientSocket != null) {
				clientSocket.close();
				clientSocket = null; 
			}
		} catch (Exception e) {
			e.printStackTrace();
			connected = false;
		}
	}
	
    public boolean sendBytes(byte[] in) {
        try
        {
            clientSocket.getOutputStream().write(intToBytes(in.length));
            clientSocket.getOutputStream().write(in, 0, in.length);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }

    public byte[] receiveBytes() {
        byte[] bts;
        try {
            byte[] lenbts = new byte[2];
            clientSocket.getInputStream().read(lenbts);
            int lenBts = bytesToInt(lenbts);
            bts = new byte[lenBts];
            int readbts = 0;
            while (readbts < bts.length) {
            	readbts += clientSocket.getInputStream().read(bts, readbts, bts.length - readbts);
            }
        } catch (IOException e) {
            e.printStackTrace();
            bts = new byte[0];
        }
        return bts;
    }
	
	public int bytesToInt(byte[] bts, int off) {
		return bts[0 + off] << (8 & 0xff) | (bts[1 + off] & 0xff);
	}
	
	public int bytesToInt(byte[] bts) {
		return bytesToInt(bts, 0);
	}
		
	public byte[] intToBytes(int in) {
		byte[] bts = new byte[2];
		bts[1] = (byte)(in);
		bts[0] = (byte)(in >> 8);
		return bts;
	}
}
