package data;

import java.io.IOException;

import logic.Figur;

public class Listen extends Thread {
	private Netzwerk con; //'P
	Listen(Netzwerk con, String name)
	{
		super(name);
		this.con = con;
	}

	@Override
	public void run() {
		while(con.isConnected()) {
			try {
				if (con.getClientSocket().getInputStream().available() > 0) {
					byte[] empfBts = con.receiveBytes();
					if (empfBts.length > 0) {
					String empf = new String(empfBts);
					decideAction(empf);
					} else {
						con.disconnect();
						return;
					}
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	private void decideAction(String input) {
		switch (input) {
		case "zug":
			zug();
			break;
		case "punkte":
			punkte();
			break;
		}
	}

	private void zug() {
		int[] pos1 = {con.bytesToInt(con.receiveBytes()), con.bytesToInt(con.receiveBytes())};
		int[] pos2 = {con.bytesToInt(con.receiveBytes()), con.bytesToInt(con.receiveBytes())};
		Figur figur = con.getLogic().getSpiel().getBrett().getFelder()[pos1[0]][pos1[1]].getFigur();
		if (figur != null) {
			con.getLogic().getSpiel().getBrett().setBewegungsmoeglichkeiten(figur.getBewegungsmoeglichkeiten(pos1[0], pos1[1]));
			con.getLogic().getSpiel().zug(con.getLogic().getSpiel().getAktSpieler(), figur, pos2[0], pos2[1]);
		}
	}
	
	private void punkte() {
		con.getLogic().getFeldUI().setPunkte(con.bytesToInt(con.receiveBytes()));
	}
}
