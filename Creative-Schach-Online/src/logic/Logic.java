package logic;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import data.Netzwerk;
import gui.Login;
import gui.Schachbrett;

public class Logic {
	private Spiel spiel;
	private Login login;
	private Netzwerk netzwerk;
	public Netzwerk getNetzwerk() {
		return netzwerk;
	}

	public Spiel getSpiel() {
		return spiel;
	}

	public Login getLogin() {
		return login;
	}

	public Schachbrett getFeldUI() {
		return feldUI;
	}

	private Schachbrett feldUI;
	private static Logic logic;
	public static void main(String args[]) {
		logic = new Logic();
		logic.spiel = new Spiel(logic);
		logic.login = new Login();
		logic.netzwerk = new Netzwerk(logic);
		new LoginListener(logic.login, logic);
		//logic.login.setSize(800, 600);
		logic.login.setVisible(true);
		logic.feldUI = new Schachbrett();
		logic.feldUI.addComponentListener(new SchachbrettListener(logic.spiel.getBrett(), logic));
		logic.feldUI.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				System.out.println("Brett Close");
				logic.exit();
			}
		});
		logic.feldUI.setSize(800, 600);
		logic.login.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				logic.feldUI.setVisible(true);
				logic.createBrett();
				logic.getNetzwerk().sendBytes("sendPunkte".getBytes());
			}
		});
	}
	
	public void createBrett() {
		Feld[][] felder = spiel.getBrett().getFelder();
		for (int iy = 0; iy < felder.length; iy++) {
			for (int ix = 0; ix < felder[iy].length; ix++) {
				logic.feldUI.getBrett().add(felder[ix][iy]);
			}
		}
	}
	
	public void resizeBrettHandle() {
		
	}
	
	public void exit() {
		System.exit(0);
	}
}
