package logic;

import java.awt.Image;
import java.util.ArrayList;

public class Figur {
	private Image bild;
	private Spieler spieler;
	private int typ = 0;
	private int wert = 0;
	private int[] pos = {0, 0};
	//private boolean bewegungsMatrix[][] = new boolean[16][16];
	
	public int getTyp() {
		return typ;
	}

	public int[] getPos() {
		return pos;
	}

	public void setPos(int[] pos) {
		this.pos = pos;
	}

	public int getWert() {
		return wert;
	}


	
	public Spieler getSpieler() {
		return spieler;
	}

	public void setSpieler(Spieler spieler) {
		this.spieler = spieler;
	}

	public Figur(Image bild) {
		this.bild = bild;
	}
	
	public Figur() {
		
	}
	
	public Figur(Image bild, Spieler spieler, int typ, int wert) {
		this.bild = bild;
		this.spieler = spieler;
		this.typ = typ;
		this.wert = wert;
	}
	
	public Image getBild() {
		return bild;
	}
	
	
	public int[][] getBewegungsmoeglichkeiten(int x, int y) {
		return new int[0][0];
	}

	public void setBild(Image bild) {
		this.bild = bild;
	}
	
	public int[] newPos(int x, int y) {
		return new int[]{x,y};
	}
	
	public boolean addValidPos(int[] pos, ArrayList<Integer[]> arrList) {
		if (pos[0] >= 0 && pos[0] < 8 && pos[1] >= 0 && pos[1] < 8) {
			if (spieler.getSpiel().getBrett().getFelder()[pos[0]][pos[1]].getFigur() == null) {
				arrList.add(new Integer[]{pos[0], pos[1]});
				return true;
			} else if(spieler.getSpiel().getBrett().getFelder()[pos[0]][pos[1]].getFigur().getSpieler() != spieler) {
				arrList.add(new Integer[]{pos[0], pos[1]});
				return true;
			}
		}
		return false;
	}
	
	public int[][] listToArr(ArrayList<Integer[]> arrList) {
		int[][] arr = new int[arrList.size()][2];
		for (int i = 0; i < arr.length; i++) {
			arr[i][0] = arrList.get(i)[0];
			arr[i][1] = arrList.get(i)[1];
		}
		return arr;
		
	}
}
