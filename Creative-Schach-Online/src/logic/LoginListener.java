package logic;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;

import gui.Login;

public class LoginListener {

	private Login login;
	private Logic logic;
	public LoginListener(Login login, Logic logic) {
		this.login = login;
		this.logic = logic;
		login.getBtnLogin().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				System.out.println("login pressed");
				logic.getNetzwerk().connect(login.getUsername().getText(), new String(login.getPassword().getPassword()));
				logic.getNetzwerk().sendBytes("login".getBytes());
				logic.getNetzwerk().sendBytes(logic.getNetzwerk().intToBytes(1));
				logic.getNetzwerk().sendBytes(logic.getNetzwerk().getBenutzername().getBytes());
				logic.getNetzwerk().sendBytes(logic.getNetzwerk().getPasswort().getBytes());
				if (logic.getNetzwerk().bytesToInt(logic.getNetzwerk().receiveBytes()) == 1) {
					logic.getNetzwerk().sendBytes("joinRndGame".getBytes());
					closeLoginWindow();
				}
			}			
		});
		login.getBtnLoginWithSc().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				System.out.println("SC login pressed");
				closeLoginWindow();
			}			
		});
		login.getBtnLoginAsGuest().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				System.out.println("login Gast pressed");
				logic.getNetzwerk().connect(login.getUsername().getText(), "");
				logic.getNetzwerk().sendBytes("login".getBytes());
				logic.getNetzwerk().sendBytes(logic.getNetzwerk().intToBytes(2));
				logic.getNetzwerk().sendBytes(logic.getNetzwerk().getBenutzername().getBytes());
				logic.getNetzwerk().sendBytes("joinRndGame".getBytes());
				closeLoginWindow();
			}		
		});
		login.getBtnRegistert().addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				logic.getNetzwerk().connect(login.getUsername().getText(), new String(login.getPassword().getPassword()));
				registrieren(logic.getNetzwerk().getBenutzername(), logic.getNetzwerk().getPasswort());
			}
			
		});
	}
	
	private void closeLoginWindow() {
		login.dispatchEvent(new WindowEvent(login, WindowEvent.WINDOW_CLOSING));
	}
	
	public void registrieren(String benutzername, String passwort) {
		logic.getNetzwerk().sendBytes("register".getBytes());
		logic.getNetzwerk().sendBytes(benutzername.getBytes());
		logic.getNetzwerk().sendBytes(passwort.getBytes());
		logic.getNetzwerk().sendBytes("joinRndGame".getBytes());
	}

}
