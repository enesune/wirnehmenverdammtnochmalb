package logic;

import java.awt.Image;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayList;

import javax.imageio.ImageIO;

public class Bauer extends Figur {

	public Bauer(Spieler spieler) {
		super(null, spieler, 6, 1);
		try {
			if(spieler.isWeiss()) setBild(ImageIO.read(new File("bin/gui/Figuren/Bauer_Weiss.png"))); else setBild(ImageIO.read(new File("bin/gui/Figuren/Bauer_Schwarz.png")));
		} catch(Exception e) {
			e.printStackTrace();
		}
		
	}
	
	@Override
	public int[][] getBewegungsmoeglichkeiten(int x, int y) {
		ArrayList<Integer[]> bewgList = new ArrayList<>();
		if (getSpieler().isWeiss()) {
			if (getSpieler().getSpiel().getBrett().getFelder()[x][y - 1].getFigur() == null) {
				addValidPos(newPos(x, y - 1), bewgList);
				if (y == 6) {
					addValidPos(newPos(x, y - 2), bewgList);
				}
			}
			if (x < 7) {
				if (getSpieler().getSpiel().getBrett().getFelder()[x + 1][y - 1].getFigur() != null) {
					addValidPos(newPos(x + 1, y - 1), bewgList);
				}
			}
			if (x > 0) {
				if (getSpieler().getSpiel().getBrett().getFelder()[x - 1][y - 1].getFigur() != null) {
					addValidPos(newPos(x - 1, y - 1), bewgList);
				}
			}

		} else {
			if (getSpieler().getSpiel().getBrett().getFelder()[x][y + 1].getFigur() == null) {
				addValidPos(newPos(x, y + 1), bewgList);
				if (y == 1) {
					addValidPos(newPos(x, y + 2), bewgList);
				}
			}
			if (x < 7) {
				if (getSpieler().getSpiel().getBrett().getFelder()[x + 1][y + 1].getFigur() != null) {
					addValidPos(newPos(x + 1, y + 1), bewgList);
				}
			}
			if (y > 0) {
				if (getSpieler().getSpiel().getBrett().getFelder()[x - 1][y + 1].getFigur() != null) {
					addValidPos(newPos(x - 1, y + 1), bewgList);
				}
			}
		}
		return listToArr(bewgList);
	}

}
