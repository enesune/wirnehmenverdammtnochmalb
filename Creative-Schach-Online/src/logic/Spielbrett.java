package logic;

import java.awt.Color;

public class Spielbrett {
	private Feld[][] felder = new Feld[8][8];
	private int[][] bewegungsmoeglichkeiten = new int[0][0];
	
	public int[][] getBewegungsmoeglichkeiten() {
		return bewegungsmoeglichkeiten;
	}

	public Feld[][] getFelder() {
		return felder;
	}
	
	public void init(Spiel spiel) {
		boolean hell = true;
		System.out.println(felder.length + " / " + felder[0].length);
		for (int iy = 0; iy < felder.length; iy++) {
			for (int ix = 0; ix < felder[iy].length; ix++) {
				felder[ix][iy] = new Feld(ix, iy);
				felder[ix][iy].setBeschriftung(calculateBeschriftung((byte)ix, (byte)iy));
				felder[ix][iy].createMouseListener(spiel);
				if (hell) {
					felder[ix][iy].setDefaultColor(Color.lightGray);
				} else {
					felder[ix][iy].setDefaultColor(Color.blue);
				}
				hell = !hell;
			}
			hell = !hell;
		}
	}
	
	public void platziereFiguren(Spieler weiss, Spieler schwarz) {
		felder[0][0].setFigur(new Turm(schwarz));
		felder[1][0].setFigur(new Springer(schwarz));
		felder[2][0].setFigur(new Laeufer(schwarz));
		felder[3][0].setFigur(new Koenig(schwarz));
		felder[4][0].setFigur(new Dame(schwarz));
		felder[5][0].setFigur(new Laeufer(schwarz));
		felder[6][0].setFigur(new Springer(schwarz));
		felder[7][0].setFigur(new Turm(schwarz));
		
		felder[0][7].setFigur(new Turm(weiss));
		felder[1][7].setFigur(new Springer(weiss));
		felder[2][7].setFigur(new Laeufer(weiss));
		felder[3][7].setFigur(new Koenig(weiss));
		felder[4][7].setFigur(new Dame(weiss));
		felder[5][7].setFigur(new Laeufer(weiss));
		felder[6][7].setFigur(new Springer(weiss));
		felder[7][7].setFigur(new Turm(weiss));
		
		for (int i = 0; i < felder[0].length; i++) {
			felder[i][1].setFigur(new Bauer(schwarz));
			felder[i][6].setFigur(new Bauer(weiss));
		}
	}
	
	public void setBewegungsmoeglichkeiten(int[][] bewgArr) {
		bewegungsmoeglichkeiten = bewgArr;
		for (int i = 0; i < bewgArr.length; i++) {
			felder[bewgArr[i][0]][bewgArr[i][1]].setColor(Color.green);
		}
	}
	
	public void brettNeuZeichnen() {
		for (int iy = 0; iy < felder.length; iy++) {
			for (int ix = 0; ix < felder[iy].length; ix++) {
				felder[ix][iy].invalidate();
				felder[ix][iy].repaint();
			}
		}
	}
	
	public String calculateBeschriftung(byte x, byte y) {
		String ret = "";
		switch (x) {
			case 0:
			break;
		}
		ret += (char)(x + (byte)65);
		ret +=  8 - y;
		
		return ret;
	}
	
	public void resetColors() {
		for (int iy = 0; iy < felder.length; iy++) {
			for (int ix = 0; ix < felder[iy].length; ix++) {
				felder[ix][iy].resetColorToDefault();
			}
		}
	}
	
	public int[] getFigurenVerhaeltniss() {
		int countWeiss = 0;
		int countSchwarz = 0;
		int koenigVer = 0;
		for (int iy = 0; iy < felder.length; iy++) {
			for (int ix = 0; ix < felder[iy].length; ix++) {
				if (felder[ix][iy].getFigur() != null) {
					if (felder[ix][iy].getFigur().getWert() < 10) {
						if (felder[ix][iy].getFigur().getSpieler().isWeiss()) {
							countWeiss += felder[ix][iy].getFigur().getWert();
						} else {
							countSchwarz += felder[ix][iy].getFigur().getWert();
						}
					} else {
						if (felder[ix][iy].getFigur().getSpieler().isWeiss()) {
							koenigVer += felder[ix][iy].getFigur().getWert();
						} else {
							koenigVer -= felder[ix][iy].getFigur().getWert();
						}
					}
				}
			}
		}
		return new int[]{countWeiss, countSchwarz, koenigVer};
	}
}
