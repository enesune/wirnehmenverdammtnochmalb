package logic;

import java.awt.Image;
import java.io.File;
import java.util.ArrayList;

import javax.imageio.ImageIO;

public class Springer extends Figur {

	public Springer(Spieler spieler) {
		super(null, spieler, 5, 3);
		try {
			if(spieler.isWeiss()) setBild(ImageIO.read(new File("bin/gui/Figuren/Springer_Weiss.png"))); else setBild(ImageIO.read(new File("bin/gui/Figuren/Springer_Schwarz.png")));
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	@Override	
	public int[][] getBewegungsmoeglichkeiten(int x, int y) {
		ArrayList<Integer[]> bewgList = new ArrayList<>();
				addValidPos(newPos(x + 2, y - 1), bewgList);
				addValidPos(newPos(x + 2, y + 1), bewgList);

				addValidPos(newPos(x - 2, y + 1), bewgList);
				addValidPos(newPos(x - 2, y - 1), bewgList);

				addValidPos(newPos(x + 1, y - 2), bewgList);
				addValidPos(newPos(x - 1, y - 2), bewgList);

				addValidPos(newPos(x + 1, y + 2), bewgList);
				addValidPos(newPos(x - 1, y + 2), bewgList);
		return listToArr(bewgList);
	}

}
