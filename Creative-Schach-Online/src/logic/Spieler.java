package logic;

public class Spieler {
	private boolean weiss = true;
	private Spiel spiel;
	private boolean benutzer;
	
	public boolean isBenutzer() {
		return benutzer;
	}

	public void setBenutzer(boolean benutzer) {
		this.benutzer = benutzer;
	}

	public Spieler(Spiel spiel) {
		this.spiel = spiel;
	}

	public Spiel getSpiel() {
		return spiel;
	}

	public void setSpiel(Spiel spiel) {
		this.spiel = spiel;
	}

	public boolean isWeiss() {
		return weiss;
	}

	public void setWeiss(boolean weiss) {
		this.weiss = weiss;
	}
}
