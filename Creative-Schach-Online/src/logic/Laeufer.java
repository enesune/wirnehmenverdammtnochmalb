package logic;

import java.awt.Image;
import java.io.File;
import java.util.ArrayList;

import javax.imageio.ImageIO;

public class Laeufer extends Figur {

	public Laeufer(Spieler spieler) {
		super(null, spieler, 4, 3);
		try {
			if(spieler.isWeiss()) setBild(ImageIO.read(new File("bin/gui/Figuren/L�ufer_Weiss.png"))); else setBild(ImageIO.read(new File("bin/gui/Figuren/L�ufer_Schwarz.png")));
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	@Override
	public int[][] getBewegungsmoeglichkeiten(int x, int y) {
		ArrayList<Integer[]> bewgList = new ArrayList<>();
			for (int i = -8; i < 8; i++) {
				addValidPos(newPos(x - i, y - i), bewgList);
				addValidPos(newPos(x - i, y + i), bewgList);
			}
		return listToArr(bewgList);
	}
}
