package logic;

import java.awt.Image;
import java.io.File;
import java.util.ArrayList;

import javax.imageio.ImageIO;

public class Turm extends Figur {

	public Turm(Spieler spieler) {
		super(null, spieler, 3, 5);
		try {
			if(spieler.isWeiss()) setBild(ImageIO.read(new File("bin/gui/Figuren/Turm_Weiss.png"))); else setBild(ImageIO.read(new File("bin/gui/Figuren/Turm_Schwarz.png")));
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	@Override
	public int[][] getBewegungsmoeglichkeiten(int x, int y) {
		ArrayList<Integer[]> bewgList = new ArrayList<>();
			for (int i = -8; i < 8; i++) {
				addValidPos(newPos(x, y - i), bewgList);
				addValidPos(newPos(x - i, y), bewgList);
			}
		return listToArr(bewgList);
	}
}
