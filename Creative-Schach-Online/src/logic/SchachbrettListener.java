package logic;

import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;

import javax.swing.JPanel;

import gui.Schachbrett;

public class SchachbrettListener extends ComponentAdapter {
	private Spielbrett logicBrett;
	private Logic logic;
	public SchachbrettListener(Spielbrett logicBrett, Logic logic) {
		this.logicBrett = logicBrett;
		this.logic = logic;
	}
	public void componentResized(ComponentEvent e) {
		if (logic.getFeldUI().getBrett().getParent().getHeight() < logic.getFeldUI().getBrett().getParent().getWidth()) {
			logic.getFeldUI().getBrett().setSize(logic.getFeldUI().getBrett().getParent().getHeight(), logic.getFeldUI().getBrett().getParent().getHeight());
		} else if (logic.getFeldUI().getBrett().getParent().getHeight() > logic.getFeldUI().getBrett().getParent().getWidth()) {
			logic.getFeldUI().getBrett().setSize(logic.getFeldUI().getBrett().getParent().getWidth(), logic.getFeldUI().getBrett().getParent().getWidth());			
		}
		logicBrett.brettNeuZeichnen();
		logic.getFeldUI().getBrett().doLayout();
		logic.getFeldUI().getBrett().invalidate();
	}
	
	public void componentShown(ComponentEvent e) {
		//logic.getNetzwerk().sendBytes("sendPunkte".getBytes());
	}
	

}
