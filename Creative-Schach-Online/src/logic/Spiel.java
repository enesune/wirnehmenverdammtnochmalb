package logic;

import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

public class Spiel {
	private Spieler[] spielerArr = new Spieler[2];
	private Spielbrett brett = new Spielbrett();
	private Spieler aktSpieler;
	private Figur selectedFigur;
	private Logic logic;
	private int zuege = 0;
	private Date zugZeit = new Date();
	
	//Getter und Setter
	public Figur getSelectedFigur() {
		return selectedFigur;
	}

	public Logic getLogic() {
		return logic;
	}

	public void setLogic(Logic logic) {
		this.logic = logic;
	}

	public void setSelectedFigur(Figur selectedFigur) {
		this.selectedFigur = selectedFigur;
	}
	
	public Spieler getAktSpieler() {
		return aktSpieler;
	}

	public void setAktSpieler(Spieler aktSpieler) {
		this.aktSpieler = aktSpieler;
	}

	public Spieler[] getSpielerArr() {
		return spielerArr;
	}

	public Spielbrett getBrett() {
		return brett;
	}

	//Getter und Setter Ende
	public Spiel(Logic logic) {
		this.logic = logic;
		init();
	}
	
	public void init() {
		brett.init(this);
		spielerArr[0] = new Spieler(this);
		spielerArr[0].setWeiss(true);
		
		spielerArr[1] = new Spieler(this);
		spielerArr[1].setWeiss(false);
		aktSpieler = spielerArr[0];
		brett.platziereFiguren(spielerArr[0], spielerArr[1]);
		Timer tmr = new Timer();
		tmr.schedule(new TimerTask() {

			@Override
			public void run() {
				if (logic.getFeldUI() != null) {
					int sekunden = (int)(new Date().getTime() - zugZeit.getTime());
					logic.getFeldUI().setZeit(sekunden / 1000);
				}
			}
			
		}, 1000, 1000);
	}
	
	public boolean zug(Spieler spieler, Figur figur, int x, int y) {
		if (figur != null) {
			for (int i = 0; i < brett.getBewegungsmoeglichkeiten().length; i++) {
				if(brett.getBewegungsmoeglichkeiten()[i][0] == x && brett.getBewegungsmoeglichkeiten()[i][1] == y) {
					logic.getFeldUI().setSpielhistorie(brett.getFelder()[figur.getPos()[0]][figur.getPos()[1]].getBeschriftung() + " --> " + brett.getFelder()[x][y].getBeschriftung());
					brett.getFelder()[figur.getPos()[0]][figur.getPos()[1]].setFigur(null);
					brett.getFelder()[x][y].setFigur(figur);
					brett.resetColors();
					brett.brettNeuZeichnen();
					if (aktSpieler == spielerArr[0]) aktSpieler = spielerArr[1]; else aktSpieler = spielerArr[0];
					selectedFigur = null;
					zuege += 1;
					logic.getFeldUI().setZuege(zuege);
					int[] figVer = brett.getFigurenVerhaeltniss();
					logic.getFeldUI().setProgressBar(((figVer[1] - figVer[0] - figVer[2]) * (100 / ((figVer[0] + figVer[1]) / 2))));
					zugZeit = new Date();
					return true;
					//break;
				}
			}
		}
		return false;
	}
	
	public boolean sendZug(Spieler spieler, Figur figur, int x, int y) {
		if (figur != null) {
			if (spieler.isBenutzer() == false) {
				if (getGegner(spieler).isBenutzer()) {
					return false;
				}
				spieler.setBenutzer(true);
			}
			logic.getNetzwerk().sendBytes("zug".getBytes());
			logic.getNetzwerk().sendBytes(logic.getNetzwerk().intToBytes(figur.getPos()[0]));
			logic.getNetzwerk().sendBytes(logic.getNetzwerk().intToBytes(figur.getPos()[1]));
			logic.getNetzwerk().sendBytes(logic.getNetzwerk().intToBytes(x));
			logic.getNetzwerk().sendBytes(logic.getNetzwerk().intToBytes(y));
		}
		if (zug(spieler, figur, x, y) == false) return false;
		return true;
	}
	
	public Spieler getGegner(Spieler spi) {
		if (spi == spielerArr[1]) return spielerArr[0];
		return spielerArr[1];
	}
	
	public void spielEnde(Spieler spieler) {
		
	}
}
