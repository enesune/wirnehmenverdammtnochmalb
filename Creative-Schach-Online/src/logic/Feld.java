package logic;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JPanel;

public class Feld extends JPanel {
	private Color defaultColor;
	private Color color;
	private Figur figur;
	private String beschriftung = "";
	private int[] pos = {0, 0};
	
	public Feld() {
		init();
	}
	
	public Feld(int x, int y) {
		pos[0] = x;
		pos[1] = y;
		init();
	}
	
	public void init() {

	}
	public void createMouseListener(Spiel spiel) {
		this.addMouseListener(new MouseAdapter() {

			@Override
			public void mouseClicked(MouseEvent arg0) {
				if (figur != null) {
					if (spiel.getAktSpieler().equals(figur.getSpieler())) {
						spiel.setSelectedFigur(figur);
						spiel.getBrett().resetColors();
						spiel.getBrett().setBewegungsmoeglichkeiten(figur.getBewegungsmoeglichkeiten(figur.getPos()[0], figur.getPos()[1]));
						spiel.getBrett().brettNeuZeichnen();
					} else {
						spiel.sendZug(spiel.getAktSpieler(), spiel.getSelectedFigur(), pos[0], pos[1]);						
					}
				} else {
					spiel.sendZug(spiel.getAktSpieler(), spiel.getSelectedFigur(), pos[0], pos[1]);				
				}
			}
		});		
	}
	public String getBeschriftung() {
		return beschriftung;
	}


	public void setBeschriftung(String beschriftung) {
		this.beschriftung = beschriftung;
	}


	public Figur getFigur() {
		return figur;
	}


	public void setFigur(Figur figur) {
		this.figur = figur;
		if (figur != null) {
			figur.setPos(new int[]{pos[0], pos[1]});
		}
	}


	public Color getColor() {
		return color;
	}


	public void setColor(Color color) {
		this.color = color;
	}
	
	public Color getDefaultColor() {
		return defaultColor;
	}


	public void setDefaultColor(Color defaultColor) {
		this.defaultColor = defaultColor;
		this.color = defaultColor;
	}
	
	public void resetColorToDefault() {
		color = defaultColor;
	}


	@Override
	public void paintComponent(Graphics g) {
		g.setColor(color);
		g.fillRect(1, 1, this.getWidth() - 1, getHeight() - 1);
		g.setColor(Color.black);
		g.drawRect(0, 0, getWidth(), getHeight());
		if (figur != null) {
			if (figur.getBild() != null) {
				g.drawImage(figur.getBild(), 0, 0, (int)((float)figur.getBild().getWidth(null) / (float)figur.getBild().getHeight(null) * this.getWidth()), this.getHeight(), this);
			} else {
				g.drawString(Integer.toString(figur.getTyp()), this.getWidth() / 2, this.getHeight() / 2);
			}
		}
		g.setColor(Color.yellow);
		g.drawString(beschriftung, this.getWidth() - 20, this.getHeight() - 10);
	}
	
}
