package logic;

import java.io.File;
import java.util.ArrayList;

import javax.imageio.ImageIO;

public class Koenig extends Figur {
	public Koenig(Spieler spieler) {
		super(null, spieler, 1, 1000);
		try {
			if(spieler.isWeiss()) setBild(ImageIO.read(new File("bin/gui/Figuren/K�nig_Weiss.png"))); else setBild(ImageIO.read(new File("bin/gui/Figuren/K�nig_Schwarz.png")));
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	@Override
	public int[][] getBewegungsmoeglichkeiten(int x, int y) {
		ArrayList<Integer[]> bewgList = new ArrayList<>();
			for (int i = -1; i < 2; i++) {
				addValidPos(newPos(x - i, y - i), bewgList);
				addValidPos(newPos(x - i, y + i), bewgList);
				addValidPos(newPos(x, y - i), bewgList);
				addValidPos(newPos(x - i, y), bewgList);
			}
		return listToArr(bewgList);
	}
}
