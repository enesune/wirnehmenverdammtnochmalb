package gui;

//import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.imageio.ImageIO;
import javax.print.DocFlavor.URL;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;
import java.awt.SystemColor;
import java.awt.Color;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JTextField;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;

public class Login extends JFrame {
	private static final long serialVersionUID = 2303911575322895620L;
	private JPanel contentPane;
	private JLabel lblLogin;
	int posX=0,posY=0;
	private JTextField textfieldUser;
	private JPasswordField textFieldPassword;
	private JButton btnLoginWithSc;
	private JButton btnLoginAsGuest;
	private JButton btnLogin;
	private JButton btnRegister;
	
	
	/**
	 * Create the frame.
	 */
	public Login() {
		setUndecorated(true);
		//setOpacity(0.9f);
		setTitle("Creative-Schach-Online");
		
		setBounds(100, 100, 440, 230);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(119, 136, 153));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		//draging this undecorated window
		addMouseListener(new MouseAdapter(){
		   public void mousePressed(MouseEvent e){
		      posX=e.getX();
		      posY=e.getY();
		   }
		});
		
		this.addMouseMotionListener(new MouseAdapter(){
		     public void mouseDragged(MouseEvent evt){
				//sets frame position when mouse dragged			
				setLocation (evt.getXOnScreen()-posX,evt.getYOnScreen()-posY);
			}
		});
		GridBagLayout gbl_contentPane = new GridBagLayout();
		gbl_contentPane.columnWidths = new int[]{62, 120, 13, 101, 50, 0, 0, 0};
		gbl_contentPane.rowHeights = new int[]{57, 42, 26, 42, 46, 0};
		gbl_contentPane.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		gbl_contentPane.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		contentPane.setLayout(gbl_contentPane);
		
		JPanel panel = new JPanel();
		panel.setBackground(new Color(46, 139, 87));
		panel.setForeground(SystemColor.desktop);
		GridBagConstraints gbc_panel = new GridBagConstraints();
		gbc_panel.fill = GridBagConstraints.BOTH;
		gbc_panel.insets = new Insets(0, 0, 0, 5);
		gbc_panel.gridheight = 5;
		gbc_panel.gridx = 0;
		gbc_panel.gridy = 0;
		contentPane.add(panel, gbc_panel);
		
		JLabel lblNewLabel = new JLabel("LOG IN ");
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setFont(new Font("Wide Latin", Font.PLAIN, 28));
		GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
		gbc_lblNewLabel.fill = GridBagConstraints.BOTH;
		gbc_lblNewLabel.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel.gridwidth = 4;
		gbc_lblNewLabel.gridx = 1;
		gbc_lblNewLabel.gridy = 0;
		contentPane.add(lblNewLabel, gbc_lblNewLabel);
		
		JButton btnClose = new JButton("");
		btnClose.setSelectedIcon(null);
		btnClose.setIcon(new ImageIcon(Serverlist.class.getResource("/gui/close_button.png")));
		//btnClose.
		btnClose.setForeground(new Color(255, 0, 0));
		btnClose.setBackground(new Color(255, 0, 0));
		btnClose.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent arg0) {
				System.exit(0);
			}
		});
		GridBagConstraints gbc_btnClose = new GridBagConstraints();
		gbc_btnClose.anchor = GridBagConstraints.WEST;
		gbc_btnClose.insets = new Insets(0, 0, 5, 5);
		gbc_btnClose.gridx = 5;
		gbc_btnClose.gridy = 0;
		contentPane.add(btnClose, gbc_btnClose);
		//end of this draging window thing
		//Quelle: https://www.codeproject.com/Tips/282494/Dragging-a-borderless-frame-in-Java
		
		
		lblLogin = new JLabel("username:");
		lblLogin.setHorizontalAlignment(SwingConstants.CENTER);
		lblLogin.setFont(new Font("Comic Sans MS", Font.BOLD, 18));
		GridBagConstraints gbc_lblLogin = new GridBagConstraints();
		gbc_lblLogin.fill = GridBagConstraints.BOTH;
		gbc_lblLogin.insets = new Insets(0, 0, 5, 5);
		gbc_lblLogin.gridx = 1;
		gbc_lblLogin.gridy = 1;
		contentPane.add(lblLogin, gbc_lblLogin);
		
		textfieldUser = new JTextField();
		textfieldUser.setColumns(10);
		GridBagConstraints gbc_textField = new GridBagConstraints();
		gbc_textField.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField.insets = new Insets(0, 0, 5, 5);
		gbc_textField.gridwidth = 3;
		gbc_textField.gridx = 2;
		gbc_textField.gridy = 1;
		contentPane.add(textfieldUser, gbc_textField);
		
		JLabel lblPassword = new JLabel("password:");
		lblPassword.setHorizontalAlignment(SwingConstants.CENTER);
		lblPassword.setFont(new Font("Comic Sans MS", Font.BOLD, 18));
		GridBagConstraints gbc_lblPassword = new GridBagConstraints();
		gbc_lblPassword.anchor = GridBagConstraints.NORTH;
		gbc_lblPassword.fill = GridBagConstraints.HORIZONTAL;
		gbc_lblPassword.insets = new Insets(0, 0, 5, 5);
		gbc_lblPassword.gridx = 1;
		gbc_lblPassword.gridy = 2;
		contentPane.add(lblPassword, gbc_lblPassword);
		
		textFieldPassword = new JPasswordField();
		textFieldPassword.setColumns(10);
		GridBagConstraints gbc_textField_1 = new GridBagConstraints();
		gbc_textField_1.anchor = GridBagConstraints.SOUTH;
		gbc_textField_1.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField_1.insets = new Insets(0, 0, 5, 5);
		gbc_textField_1.gridwidth = 3;
		gbc_textField_1.gridx = 2;
		gbc_textField_1.gridy = 2;
		contentPane.add(textFieldPassword, gbc_textField_1);
		
		btnRegister = new JButton("register");
		GridBagConstraints gbc_btnRegister = new GridBagConstraints();
		gbc_btnRegister.insets = new Insets(0, 0, 5, 5);
		gbc_btnRegister.gridx = 3;
		gbc_btnRegister.gridy = 3;
		contentPane.add(btnRegister, gbc_btnRegister);
		
		btnLoginWithSc = new JButton("Login with SC account");
		GridBagConstraints gbc_btnLoginWithSc = new GridBagConstraints();
		gbc_btnLoginWithSc.anchor = GridBagConstraints.NORTHWEST;
		gbc_btnLoginWithSc.insets = new Insets(0, 0, 0, 5);
		gbc_btnLoginWithSc.gridwidth = 2;
		gbc_btnLoginWithSc.gridx = 1;
		gbc_btnLoginWithSc.gridy = 4;
		contentPane.add(btnLoginWithSc, gbc_btnLoginWithSc);
		
		btnLoginAsGuest = new JButton("Login as guest");
		GridBagConstraints gbc_btnLoginAsGuest = new GridBagConstraints();
		gbc_btnLoginAsGuest.anchor = GridBagConstraints.NORTHWEST;
		gbc_btnLoginAsGuest.insets = new Insets(0, 0, 0, 5);
		gbc_btnLoginAsGuest.gridx = 3;
		gbc_btnLoginAsGuest.gridy = 4;
		contentPane.add(btnLoginAsGuest, gbc_btnLoginAsGuest);
		
		btnLogin = new JButton("Login");
		btnLogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		GridBagConstraints gbc_btnNewButton = new GridBagConstraints();
		gbc_btnNewButton.anchor = GridBagConstraints.NORTHWEST;
		gbc_btnNewButton.gridwidth = 3;
		gbc_btnNewButton.gridx = 4;
		gbc_btnNewButton.gridy = 4;
		contentPane.add(btnLogin, gbc_btnNewButton);
		
	}
	
	public JTextField getUsername() {
    	return this.textfieldUser;
    }
	
	public JPasswordField getPassword() {
    	return this.textFieldPassword;
    }
	
	public JButton getBtnLoginWithSc() {
    	return this.btnLoginWithSc;
    }
	
	public JButton getBtnLoginAsGuest() {
    	return this.btnLoginAsGuest;
    }
	
	public JButton getBtnRegistert() {
    	return this.btnRegister;
    }
	
	public JButton getBtnLogin() {
    	return this.btnLogin;
    }
}