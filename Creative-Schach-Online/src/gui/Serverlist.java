package gui;

import javax.swing.JFrame;
import java.awt.GridLayout;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.JList;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import javax.swing.JButton;
import javax.swing.JToggleButton;
import javax.swing.JCheckBox;
import javax.swing.JPasswordField;

public class Serverlist extends JFrame{
	
	
	private JTextField gamename;
	private JPasswordField passwordField;
	private JList gamelist;
	private JButton btnCreateGame;
	private JCheckBox isPrivate;
	private JButton btnStartGame;
	
	
	
	
	public Serverlist() {

		setTitle("Creative-Schach-Online Serverlist");
		setSize(400, 260);
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{6, 113, 0, 70, 72, 0};
		gridBagLayout.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 28, 0, 0};
		gridBagLayout.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		getContentPane().setLayout(gridBagLayout);
		
		JLabel lblSpielliste = new JLabel("Gamelist:");
		GridBagConstraints gbc_lblSpielliste = new GridBagConstraints();
		gbc_lblSpielliste.insets = new Insets(0, 0, 5, 5);
		gbc_lblSpielliste.gridx = 1;
		gbc_lblSpielliste.gridy = 1;
		getContentPane().add(lblSpielliste, gbc_lblSpielliste);
		
		JList list = new JList();
		GridBagConstraints gbc_list = new GridBagConstraints();
		gbc_list.gridheight = 6;
		gbc_list.insets = new Insets(0, 0, 0, 5);
		gbc_list.fill = GridBagConstraints.BOTH;
		gbc_list.gridx = 1;
		gbc_list.gridy = 2;
		getContentPane().add(list, gbc_list);
		
		JLabel lblGamename = new JLabel("gamename:");
		GridBagConstraints gbc_lblGamename = new GridBagConstraints();
		gbc_lblGamename.anchor = GridBagConstraints.EAST;
		gbc_lblGamename.insets = new Insets(0, 0, 5, 5);
		gbc_lblGamename.gridx = 2;
		gbc_lblGamename.gridy = 2;
		getContentPane().add(lblGamename, gbc_lblGamename);
		
		gamename = new JTextField();
		GridBagConstraints gbc_textField = new GridBagConstraints();
		gbc_textField.gridwidth = 2;
		gbc_textField.insets = new Insets(0, 0, 5, 0);
		gbc_textField.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField.gridx = 3;
		gbc_textField.gridy = 2;
		getContentPane().add(gamename, gbc_textField);
		gamename.setColumns(10);
		
		JButton btnCreateGame = new JButton("create game");
		GridBagConstraints gbc_btnStartGame = new GridBagConstraints();
		gbc_btnStartGame.gridwidth = 2;
		gbc_btnStartGame.insets = new Insets(0, 0, 5, 0);
		gbc_btnStartGame.gridx = 3;
		gbc_btnStartGame.gridy = 3;
		getContentPane().add(btnCreateGame, gbc_btnStartGame);
		
		JLabel lblPrivateGame = new JLabel("private:");
		GridBagConstraints gbc_lblPrivateGame = new GridBagConstraints();
		gbc_lblPrivateGame.anchor = GridBagConstraints.EAST;
		gbc_lblPrivateGame.insets = new Insets(0, 0, 5, 5);
		gbc_lblPrivateGame.gridx = 2;
		gbc_lblPrivateGame.gridy = 4;
		getContentPane().add(lblPrivateGame, gbc_lblPrivateGame);
		
		JCheckBox checkBox = new JCheckBox("");
		GridBagConstraints gbc_checkBox = new GridBagConstraints();
		gbc_checkBox.anchor = GridBagConstraints.WEST;
		gbc_checkBox.insets = new Insets(0, 0, 5, 5);
		gbc_checkBox.gridx = 3;
		gbc_checkBox.gridy = 4;
		getContentPane().add(checkBox, gbc_checkBox);
		
		JLabel lblPassword = new JLabel("password:");
		GridBagConstraints gbc_lblPassword = new GridBagConstraints();
		gbc_lblPassword.anchor = GridBagConstraints.EAST;
		gbc_lblPassword.insets = new Insets(0, 0, 5, 5);
		gbc_lblPassword.gridx = 2;
		gbc_lblPassword.gridy = 5;
		getContentPane().add(lblPassword, gbc_lblPassword);
		
		passwordField = new JPasswordField();
		GridBagConstraints gbc_passwordField = new GridBagConstraints();
		gbc_passwordField.gridwidth = 2;
		gbc_passwordField.insets = new Insets(0, 0, 5, 0);
		gbc_passwordField.fill = GridBagConstraints.HORIZONTAL;
		gbc_passwordField.gridx = 3;
		gbc_passwordField.gridy = 5;
		getContentPane().add(passwordField, gbc_passwordField);
		
		JButton btnStartGame = new JButton("start game");
		GridBagConstraints gbc_btnStartGame_1 = new GridBagConstraints();
		gbc_btnStartGame_1.insets = new Insets(0, 0, 0, 5);
		gbc_btnStartGame_1.gridx = 3;
		gbc_btnStartGame_1.gridy = 7;
		getContentPane().add(btnStartGame, gbc_btnStartGame_1);
	}
	
	public JTextField getGamename() {
		return gamename;
	}


	public void setGamename(JTextField gamename) {
		this.gamename = gamename;
	}


	public JPasswordField getPasswordField() {
		return passwordField;
	}


	public void setPasswordField(JPasswordField passwordField) {
		this.passwordField = passwordField;
	}


	public JList getGamelist() {
		return gamelist;
	}


	public void setGamelist(JList gamelist) {
		this.gamelist = gamelist;
	}


	public JButton getBtnCreateGame() {
		return btnCreateGame;
	}


	public void setBtnCreateGame(JButton btnCreateGame) {
		this.btnCreateGame = btnCreateGame;
	}


	public JCheckBox getIsPrivate() {
		return isPrivate;
	}


	public void setIsPrivate(JCheckBox isPrivate) {
		this.isPrivate = isPrivate;
	}


	public JButton getBtnStartGame() {
		return btnStartGame;
	}


	public void setBtnStartGame(JButton btnStartGame) {
		this.btnStartGame = btnStartGame;
	}

}
