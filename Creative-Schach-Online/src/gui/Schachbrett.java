package gui;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.GridLayout;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.Color;
import javax.swing.SwingConstants;
import javax.swing.JProgressBar;
import javax.swing.DefaultListModel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;
import java.awt.Toolkit;
import javax.swing.JList;
//import java.util.Timertask;

public class Schachbrett extends JFrame { 
	
	
    JPanel feld = null; 
    boolean sw = false;     
    private final Color WHITE = new Color(255,255,255); 
    private final Color BLACK = new Color(0,0,0);  
    private JPanel brett = new JPanel();
    private JProgressBar progressBar = new JProgressBar();
    JLabel zuege = new JLabel("0");
	private JLabel labelZeit = new JLabel();
	private JLabel labelPunkte = new JLabel();
	DefaultListModel<String> model = new DefaultListModel<>();
	private JList<String> spielhistorie = new JList<>( model );
	private final JPanel panel_1 = new JPanel();
	
	
    public JPanel getBrett() {return brett;}
    
    public JProgressBar getProgressBar() {return this.progressBar;}
    
   
    
    
    
    public void setZeit(int zeit) {
    	labelZeit.setText(Integer.toString(zeit/60) + ":" + Integer.toString(zeit%60));
    }
    
    public void setProgressBar(int i) {
    	progressBar.setValue(i);
    }
    
    public void setPunkte(int punkte) {
    	labelPunkte.setText(String.valueOf(punkte));
    }
    
    public void setSpielhistorie(String spielhistorie) {
    	model.addElement(spielhistorie);
    }
    
   // public void setSpielhistorie();{ }
    
    public void setZuege(int zuege) {
    	this.zuege.setText(String.valueOf(zuege));
    } 
    
    public JLabel getZuege() {return zuege;}
    
   
    
	public Schachbrett() {
		
		setIconImage(Toolkit.getDefaultToolkit().getImage(Schachbrett.class.getResource("/gui/cso_ico.png")));
		setTitle("Creative-Schach-Online");
		
		getContentPane().add(panel_1, BorderLayout.CENTER);
		panel_1.setLayout(null);
		brett.setBounds(0, 0, 150, 150);
		panel_1.add(brett);
		brett.setLayout(new GridLayout(8, 8));
		
		JPanel panel = new JPanel();
		getContentPane().add(panel, BorderLayout.EAST);
		
		JLabel lblNewLabel = new JLabel("Z\u00FCge:");
		lblNewLabel.setVerticalAlignment(SwingConstants.TOP);
		lblNewLabel.setHorizontalAlignment(SwingConstants.LEFT);
		
		
		progressBar.setMinimum(-100);
		progressBar.setBackground(WHITE);
		progressBar.setForeground(BLACK);
		
		JLabel lblZeit = new JLabel("Zeit:");
		
		
		
		JLabel lblSpielhistorie = new JLabel("Spielhistorie:");
		
		JLabel lblDeinePunkte = new JLabel("Deine Punkte:");
		
		
		
		
		
		GroupLayout gl_panel = new GroupLayout(panel);
		gl_panel.setHorizontalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_panel.createParallelGroup(Alignment.LEADING, false)
						.addGroup(gl_panel.createSequentialGroup()
							.addComponent(lblNewLabel, GroupLayout.PREFERRED_SIZE, 44, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(zuege))
						.addComponent(progressBar, GroupLayout.PREFERRED_SIZE, 183, GroupLayout.PREFERRED_SIZE)
						.addComponent(spielhistorie, GroupLayout.PREFERRED_SIZE, 161, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblSpielhistorie)
						.addGroup(gl_panel.createSequentialGroup()
							.addComponent(lblDeinePunkte)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(labelPunkte))
						.addGroup(gl_panel.createSequentialGroup()
							.addComponent(lblZeit)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(labelZeit, GroupLayout.PREFERRED_SIZE, 155, GroupLayout.PREFERRED_SIZE)))
					.addContainerGap())
		);
		gl_panel.setVerticalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addComponent(progressBar, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblNewLabel)
						.addComponent(zuege))
					.addGap(17)
					.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblDeinePunkte)
						.addComponent(labelPunkte))
					.addGap(4)
					.addGroup(gl_panel.createParallelGroup(Alignment.LEADING, false)
						.addGroup(gl_panel.createSequentialGroup()
							.addComponent(lblZeit)
							.addGap(5))
						.addGroup(gl_panel.createSequentialGroup()
							.addComponent(labelZeit, GroupLayout.DEFAULT_SIZE, 0, Short.MAX_VALUE)
							.addPreferredGap(ComponentPlacement.RELATED)))
					.addComponent(lblSpielhistorie)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(spielhistorie, GroupLayout.PREFERRED_SIZE, 129, GroupLayout.PREFERRED_SIZE)
					.addGap(19))
		);
		panel.setLayout(gl_panel);
		
}
}
