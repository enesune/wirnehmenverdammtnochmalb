package logic;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;

public class Client {
	private OutputStream outStream;
	private int clientID = 0;
	private String clientName = "";
	private String clientPasswort = "";
	private Socket clientSocket;
	private boolean connected = true;
	private Logic logic;
	private Spiel spiel;
	
	public Client(Socket clientSocket, Logic logic) {
		this.clientSocket = clientSocket;
		this.logic = logic;
		try {
			outStream = clientSocket.getOutputStream();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public Spiel getSpiel() {
		return spiel;
	}

	public void setSpiel(Spiel spiel) {
		this.spiel = spiel;
	}

	public Logic getLogic() {
		return logic;
	}

	public boolean isConnected() {
		return connected;
	}

	public int getClientID() {
		return clientID;
	}

	public void setClientID(int clientID) {
		this.clientID = clientID;
	}

	public String getClientName() {
		return clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	public String getClientPasswort() {
		return clientPasswort;
	}

	public void setClientPasswort(String clientPasswort) {
		this.clientPasswort = clientPasswort;
	}

	public Socket getClientSocket() {
		return clientSocket;
	}

	public OutputStream getOutStream() {
		return outStream;
	}
	
	public int getId() {
		return clientID;
	}
	
	public String getName() {
		return clientName;
	}
	
	public Socket getSocket() {
		return clientSocket;
	}
	
	public void Disconnect()
	{
		try {
			clientSocket.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		connected = false;
		System.out.println(clientName + " (" + clientID + ") hat den Server verlassen."); //'"
	}
	
    public boolean sendBytes(byte[] in) {
        try
        {
            outStream.write(intToBytes(in.length));
            outStream.write(in, 0, in.length);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }

    public byte[] receiveBytes() {
        byte[] bts;
        try {
            byte[] lenbts = new byte[2];
            clientSocket.getInputStream().read(lenbts);
            int lenBts = bytesToInt(lenbts);
            bts = new byte[lenBts];
            int readbts = 0;
            while (readbts < bts.length) {
            	readbts += clientSocket.getInputStream().read(bts, readbts, bts.length - readbts);
            }
        } catch (IOException e) {
            e.printStackTrace();
            bts = new byte[0];
        }
        return bts;
    }
	
	public int bytesToInt(byte[] bts, int off) {
		return bts[0 + off] << (8 & 0xff) | (bts[1 + off] & 0xff);
	}
	
	public int bytesToInt(byte[] bts) {
		return bytesToInt(bts, 0);
	}
		
	public byte[] intToBytes(int in) {
		byte[] bts = new byte[2];
		bts[1] = (byte)(in);
		bts[0] = (byte)(in >> 8);
		return bts;
	}
}
