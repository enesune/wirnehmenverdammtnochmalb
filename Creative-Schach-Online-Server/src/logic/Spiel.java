package logic;

public class Spiel {
	private Client[] spielerArr = new Client[2];
	
	public Spiel(Client sp1, Client sp2) {
		this.spielerArr[0] = sp1;
		this.spielerArr[1] = sp2;
	}
	
	public Client getGegner(Client cli) {
		if (cli == spielerArr[1]) return spielerArr[0];
		return spielerArr[1];
	}

}
