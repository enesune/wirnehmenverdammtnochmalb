package logic;

import java.io.IOException;
import java.util.ArrayList;

import data.Datenbank;
import data.ServerData;

public class Logic {
	ServerData servDat = new ServerData();
	private Datenbank datenbank;
	private ArrayList<Client> clientList = new ArrayList<>();
	private ArrayList<Client> warteListe = new ArrayList<>();
	
	public Datenbank getDatenbank() {
		return datenbank;
	}

	public ArrayList<Client> getClientList() {
		return clientList;
	}

	public ArrayList<Client> getWarteListe() {
		return warteListe;
	}

	public void initServer() {
		servDat.init();
	}
	
	private void serverTick() {
		try {
			Client cli = new Client(servDat.getServSocket().accept(), this);
			clientList.add(cli);
			new Listen(cli, "").start();
			System.out.println("neue Verbindung!");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		Logic mainLogic = new Logic();
		System.out.println("Server startet...");
		mainLogic.datenbank = new Datenbank();
		mainLogic.datenbank.verbindungAufbauen();
		mainLogic.initServer();
		System.out.println("Server l�uft.");
		while (true) {
			mainLogic.serverTick();
			try {
				Thread.sleep(10);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

}
