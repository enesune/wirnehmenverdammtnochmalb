package logic;

import java.io.IOException;

public class Listen extends Thread {
	private Client con; //'P
	Listen(Client con, String name)
	{
		super(name);
		this.con = con;
	}

	@Override
	public void run() {
		while(con.isConnected()) {
			byte[] empfBts = con.receiveBytes();
			if (empfBts.length > 0) {
			String empf = new String(empfBts);
			decideAction(empf);
			} else {
				con.Disconnect();
				return;
			}
		}
	}
	
	public void decideAction(String input) {
		switch (input) {
		case "login":
			login();
			System.out.println("Client " + con.getClientName() + " hat verbunden.");
			break;
		case "register":
			register();
			break;
		case "joinRndGame":
			if (con.getLogic().getWarteListe().size() <= 0 ) {
				con.getLogic().getWarteListe().add(con);
			} else {
				Client gegner = con.getLogic().getWarteListe().get(0);
				newGameWith(gegner);
				con.getLogic().getWarteListe().remove(gegner);
			}
			break;
		case "zug":
			zug();
		case "sendPunkte":
			sendPunkte();
		}
	}
	
	public boolean login() {
		switch (con.bytesToInt(con.receiveBytes())) {
		case 1: //Normaler Login
			con.setClientName(new String(con.receiveBytes()));
			con.setClientPasswort(new String(con.receiveBytes()));
			if (con.getLogic().getDatenbank().login(con.getClientName(), con.getClientPasswort())) {
				con.sendBytes(con.intToBytes(1));
				return true;
			}
			con.sendBytes(con.intToBytes(0));
			try {
				con.getOutStream().flush();
			} catch (IOException e) {
				e.printStackTrace();
			}
			con.Disconnect();
			break;
		case 2: //Gast Login
			con.setClientName(new String(con.receiveBytes()));
			return true;
		case 3: //SC Login
			con.setClientName(new String(con.receiveBytes()));
			return true;
		default:
			return false;
		}
		return false;
	}
	
	public boolean register() {
		con.setClientName(new String(con.receiveBytes()));
		con.setClientPasswort(new String(con.receiveBytes()));
		if (con.getLogic().getDatenbank().register(con.getClientName(), con.getClientPasswort())) return true;
		return false;
	}
	
	public void newGameWith(Client gegner) {
		Spiel sp = new Spiel(con, gegner);
		con.setSpiel(sp);
		gegner.setSpiel(sp);
	}
	
	public void zug() {
		int[] pos1 = {con.bytesToInt(con.receiveBytes()), con.bytesToInt(con.receiveBytes())};
		int[] pos2 = {con.bytesToInt(con.receiveBytes()), con.bytesToInt(con.receiveBytes())};
		if (con.getSpiel() != null) {
			Client gegner = con.getSpiel().getGegner(con);
			if (gegner != null) {
				gegner.sendBytes("zug".getBytes());
				gegner.sendBytes(gegner.intToBytes(pos1[0]));
				gegner.sendBytes(gegner.intToBytes(pos1[1]));
				gegner.sendBytes(gegner.intToBytes(pos2[0]));
				gegner.sendBytes(gegner.intToBytes(pos2[1]));
			}
		}
	}
	
	private void sendPunkte() {
		con.sendBytes("punkte".getBytes());
		con.sendBytes(con.intToBytes(con.getLogic().getDatenbank().get_points(con.getClientName())));
	}
}
