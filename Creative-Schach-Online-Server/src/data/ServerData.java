package data;

import java.io.IOException;
import java.net.ServerSocket;

public class ServerData {
	private int port = 4555;
	private int bufferSize = 4096;
	private static ServerSocket servSocket;
	private static Datenbank datb = new Datenbank();
	
	public ServerSocket getServSocket() {
		return servSocket;
	}
	
	public void init() {
		try {
			servSocket = new ServerSocket(port);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
