package data;

import java.io.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Datenbank {

	// dangerous key_words from STDIN user input
	private final String[] danger_key_words = { "INSERT", "ALTER", "SELECT", "UPDATE", "DELETE", "DROP" };
 
	private String driver = "com.mysql.jdbc.Driver";
	private String url = "jdbc:mysql://localhost:3306/creative_schach?";
	private String user = "root";
	private String password = "";

	private Connection con;
	public boolean connected = false;

	public boolean verbindungAufbauen() {
		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, user, password);
			connected = true;

		} catch (Exception ex) {
			ex.printStackTrace();
			return false;
		}
		return true;
	}
	
	private void error(String fault) {

		System.out.println("Error occured ::: " + fault);

	}

	public String getResultsAsString(String sqlIn, int column) {

		String res = "";

		if (connected == true) {

			try {

				Statement stmt = con.createStatement();
				stmt.execute(sqlIn);
				ResultSet rs = stmt.getResultSet();

				while (rs.next()) {
					res += rs.getString(column) + " ;\n";
				}
			} catch (SQLException ex) {

				ex.printStackTrace();

			}
		}
		return res;
	}

	public String getFirstResult(String sqlIn, String column) {

		String res = "";

		if (connected == true) {

			Statement stmt;

			try {

				stmt = con.createStatement();
				stmt.execute(sqlIn);
				ResultSet rs = stmt.getResultSet();

				if (rs.next())

					res = rs.getString(column);

			} catch (SQLException e) {

				e.printStackTrace();

			}

		}
		return res;
	}

	// source, dest
	private boolean str_match(String src, String str_to_search) {

		if (src.length() == 0)

			return true;

		int pos, match = 0;

		for (pos = 0; pos < (src.length()); ++pos) {

			if (src.charAt(pos) == str_to_search.charAt(match))

				++match;

			else

				if (src.charAt(pos) == str_to_search.charAt(0))
	
					match = 0x01;
	
				else

					match = 0;
	
			if (match == str_to_search.length())
	
				return true;

		}

		return false;

	}

	// against SQL injection
	public boolean check_query(String query) {

		String tmp_key = "";

		int count;

		for (count = 0; count < (danger_key_words.length); ++count) {

			tmp_key = danger_key_words[count];

			if (str_match(query.toUpperCase(), tmp_key)) {

				error("SQL injection detected!");
				
				return false;
				
			}

		}

		return true;

	}

	public String get_u_name(int ID_user) {

		String res = "";
		if (connected == true) {

			Statement stmt;

			try {
				stmt = con.createStatement();

				// exec MySQL cmd
				stmt.execute("SELECT name FROM T_User WHERE p_UserID = " + ID_user);

				ResultSet rs = stmt.getResultSet();

				if (rs.next())

					res = rs.getString(1);

			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();

				error("in function get_u_name()");
				
				return "";

			}

		}

		return res;

	}

	public boolean set_u_name(int ID_user, String new_name) {

		if (connected == true) {

			Statement stmt;
			try {

				stmt = con.createStatement();

				stmt.execute("UPDATE T_User SET name = \"" + new_name + "\" WHERE ID_user = " + ID_user);

			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();

				error("in function set_u_name()");
				
				return false;

			}

		}

		return true;

	}

	public boolean register(String u_name, String pwd) {

		if ( (check_query(u_name) == false) || (check_query(pwd) == false )) {
		
			error("SQL Injection detected! Registration failed.");
			
			return false;
		
		}
			
		String non_safe_pwd = XOR_BLOCK(pwd);

		if (connected == true) {

			Statement stmt;

			try {

				stmt = con.createStatement();

				if (exists_u_name(u_name) == true) {

					// nutzer existiert bereits
					System.out.println("Name existiert beriets! Bitte waehle einen anderen.");
					return false;

				} else {
 
					// register
					// ID wird hier nicht gebraucht -> auto_increment
					stmt.execute(
							"INSERT INTO T_User (name, pwd, won_games) VALUES (\"" + u_name + "\",\"" + non_safe_pwd + "\", 0 );");

				}

			} catch (SQLException e) {

				e.printStackTrace();

				error("in function register()");
				
				return false;

			}

		}

		return true;

	}

	public boolean exists_u_name(String u_name) {

		if (connected == true) {

			Statement stmt;

			try {

				stmt = con.createStatement();

				stmt.execute("SELECT name FROM T_User WHERE name = \"" + u_name + "\"");

				ResultSet rs = stmt.getResultSet();

				if (!rs.next())

					return false;

			} catch (SQLException e) {

				e.printStackTrace();

				error("in function exists_u_name()");
				
				return false;

			}

		}

		return true;

	}

	public void inc_points(String u_name) {

		if (connected == true) {

			Statement stmt;

			try {

				stmt = con.createStatement();

				stmt.execute("UPDATE T_User SET won_games = won_games + 1 WHERE name = \"" + u_name + "\"");

			} catch (SQLException e) {

				error("in function inc_points");
				
				return;

			}

		}

		return;

	}

	public int get_points(String name) {

		int points = 0;

		if (connected == true) {

			Statement stmt;

			try {

				stmt = con.createStatement();

				stmt.execute("SELECT won_games FROM T_User WHERE name = \"" + name + "\"");

				ResultSet rs = stmt.getResultSet();

				if (rs.next())

					points = rs.getInt("won_games");

			} catch (SQLException e) {

				e.printStackTrace();
				
				error("in function get_points()");

			}

		}

		return points;

	}

	public boolean login(String u_name, String pwd) {

		if ( (check_query(u_name) == false) || (check_query(pwd) == false )) {
			
			error("SQL Injection detected!");
			
			return false;
		
		}
		
		String non_safe_pwd = XOR_BLOCK(pwd);

		if (connected == true) {

			Statement stmt;

			try {

				stmt = con.createStatement();

				stmt.execute("SELECT name from T_User WHERE name = \"" + u_name + "\" AND pwd = \"" + non_safe_pwd + "\"");

				ResultSet rs = stmt.getResultSet();

				if (rs.next()) {

					return true;

				} else {

					return false;

				}
			
			} catch (SQLException e) {

				e.printStackTrace();
				return false;

			}

		}

		return true;

	}
	
	private String XOR_BLOCK(String str) {
		
		String verschl = "";
		
		for(int i = 0; i < str.length() ; ++i)
			
			verschl += str.charAt(i) ^ 0xff;
		
		return verschl;
		
	}
	
	public boolean disconnect(String name) {

		if (connected == true) {

			try { 

				System.out.println("logging out...");

				con.close();

			} catch (SQLException e) {
				return false;
			}

			return true;

		}
		return false;
	}
}
